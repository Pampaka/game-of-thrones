-- роли
INSERT INTO public.roles (id, role, "createdAt", "updatedAt")
SELECT id, role, "createdAt", "updatedAt"
FROM copy.roles;

SELECT setval('public.roles_id_seq',
              (SELECT MAX(id) FROM public.roles)
           );
-- диапазон уровней
INSERT INTO public.levels (id, range, progress, "createdAt", "updatedAt")
SELECT id, range, progress, "createdAt", "updatedAt"
FROM copy.levels;

SELECT setval('public.levels_id_seq',
              (SELECT MAX(id) FROM public.levels)
           );
-- пользователи
INSERT INTO public.users (id, nickname, "currentLevel", "progressLevel", "createdAt", "updatedAt", "levelId", "roleId")
SELECT id, nickname, "currentLevel", "progressLevel", "createdAt", "updatedAt", "levelId", "roleId"
FROM copy.users;

SELECT setval('public.users_id_seq',
              (SELECT MAX(id) FROM public.users)
           );