class ApiError {

    errorHandler = (err, req, res) => {
        res.status(500);
        return res.render('error', { error: err });
    }

    errorLogger(err, req, res, next) {
        console.error(err)
        return next(err)
    }

    errorResponder(err, req, res, next) {
        if (err.type === 'redirect')
            return res.redirect('/error')
        else if (err.type === 'time-out')
            return res.status(408).send(err)
        else
            return next(err)
    }
}

module.exports = {ApiError}