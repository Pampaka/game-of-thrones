const {sequelize} = require("../db");
const {DataTypes} = require("sequelize");

const Roles = sequelize.define("role", {
    id: {type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true, allowNull: false},
    role: {type: DataTypes.STRING, allowNull: false},
})

const Users = sequelize.define("user", {
    id: {type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true, allowNull: false},
    nickname: {type: DataTypes.STRING, allowNull: false},
    currentLevel: {type: DataTypes.INTEGER, allowNull: false, defaultValue: 1},
    progressLevel: {type: DataTypes.INTEGER, allowNull: false, defaultValue: 0},
})

const Levels = sequelize.define("level", {
    id: {type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true, allowNull: false},
    range: {type: DataTypes.INTEGER, allowNull: false},
    progress: {type: DataTypes.INTEGER, allowNull: false},
})

Levels.hasMany(Users);
Users.belongsTo(Levels, {foreignKey: {allowNull: false}, onDelete: "CASCADE", hooks: true});

Roles.hasMany(Users);
Users.belongsTo(Roles, {foreignKey: {allowNull: false}, onDelete: "CASCADE", hooks: true});

module.exports = {
    Users,
    Levels
}