// подключение env
require("dotenv").config();
// база данных
const {sequelize} = require("./db");
require("./models/usersModels");
// express
const express = require('express');
const cors = require('cors');
const {routes} = require("./routes/index");
const PORT = process.env.PORT;

const app = express();
app.use(cors());
app.use(express.json());

// события реального времени (скорее всего надо объявлять для каждого контроллера)
const events = require('events');
const emitter = new events.EventEmitter();

// роутер
app.get("/", (req, res) => {
    res.send("Server ON")
});
app.use("/api", routes);

// обработка ошибок
const {ApiError} = require("./utils/ApiError");
const apiError = new ApiError();
app.use(apiError.errorLogger);
app.use(apiError.errorResponder);
app.use(apiError.errorHandler);

// запуск сервера
(async () => {
    await sequelize.sync().catch(err =>
        console.log(err)
    );

    app.listen(PORT, () => {
        console.log(`Server has been started on port ${PORT}`)
    });
})()
