import React, {useState} from 'react';
import Background from "../components/login/Background";
import FormLogin from "../components/login/FormLogin";
import FormRegistration from "../components/login/FormRegistration";

const Login = () => {
    const [formType, setFormType] = useState("login");

    return (
        <div className={"login-container"}>
            <div className={"login-form-container"}>
                {formType === "login" ?
                    <FormLogin setFormType={setFormType}/>
                    :
                    <FormRegistration setFormType={setFormType}/>
                }
            </div>
            <Background/>
        </div>
    );
};

export default Login;