import React from 'react';
import MapElement from "../components/Game/map/MapElement";

const Game = () => {


    return (
        <div className={"game-container"}>
            <MapElement/>
        </div>
    );
};

export default Game;