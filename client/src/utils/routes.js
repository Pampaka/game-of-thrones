import {ADMIN_ROUTE, GAME_ROUTE, LOGIN_ROUTE, MENU_ROUTE} from "./paths";
import Login from "../pages/Login";
import Game from "../pages/Game";
import MainMenu from "../pages/MainMenu";
import Admin from "../pages/Admin";

export const routesApp = {
    publicRoutes: [
        {
            path: LOGIN_ROUTE,
            element: Login
        },
        {
            path: GAME_ROUTE,
            element: Game
        },
        {
            path: MENU_ROUTE,
            element: MainMenu
        },
    ],
    adminRoutes: [
        {
            path: ADMIN_ROUTE,
            element: Admin
        }
    ]
}