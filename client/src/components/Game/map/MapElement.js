import React from 'react';
import {MapContainer, TileLayer} from "react-leaflet";
import Test from "./Test";
import Config from "./Config";


const MapElement = () => {
    const position = [7.536764322084078, 47.4609375];

    return (
        <MapContainer style={{height: window.innerHeight}} center={position} zoom={5}>
            <TileLayer url="https://cartocdn-gusc.global.ssl.fastly.net/ramirocartodb/api/v1/map/named/tpl_756aec63_3adb_48b6_9d14_331c6cbc47cf/all/{z}/{x}/{y}.png"/>
            <Test/>
            <Config/>
        </MapContainer>
    );
};

export default MapElement;