import React, {useEffect} from 'react';
import {useMap, useMapEvents} from "react-leaflet";

const Config = () => {
    const map = useMap();
    // границы карты
    const southEast = [-41.870204788160066, 91.92260742187501];
    const northWest = [49.07206655441654, 0.950317372402867];
    const bounds = [southEast, northWest];

    useEffect(() => {
        map.setMinZoom(5);
        map.setMaxBounds(bounds);
    }, [])

    useMapEvents({
        drag() {
            map.panInsideBounds(bounds, { animate: false });
        }
    })

    return null;
};

export default Config;