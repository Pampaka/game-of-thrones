import React from 'react';
import {useMap, useMapEvents} from "react-leaflet";

const Test = () => {
    const map = useMap();

    useMapEvents({
        click(e) {
            console.log(e);
            console.log(map.getZoom());
        }
    })

    return null;
};

export default Test;