import React from 'react';
import {Routes, Route} from "react-router";
import {routesApp} from "../utils/routes";

const AppRouter = () => {
    return (
        <Routes>
            {routesApp.publicRoutes.map(route =>
                <Route key={route.path} path={route.path} element={route.element()}/>
            )}
            {routesApp.adminRoutes.map(route =>
                <Route key={route.path} path={route.path} element={route.element()}/>
            )}
        </Routes>
    );
};

export default AppRouter;