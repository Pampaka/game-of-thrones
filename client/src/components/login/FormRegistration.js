import React from 'react';
import {FormControl} from "react-bootstrap-v5";
import {Button} from "@mui/material";

const FormRegistration = ({setFormType}) => {
    const goToLogin = () => {
        setFormType("login")
    }

    return (
        <form className={"login-form"}>
            <h1 className={"login-form-text mb-3"}>Регистрация</h1>
            <div className={"login-form-group"}>
                <FormControl className={"login-form-input mb-3"} placeholder={"Имя пользователя"}/>
                <FormControl className={"login-form-input mb-5"} placeholder={"Пароль"}/>
                <div className={"d-flex justify-content-between align-items-center m-5 mb-0 mt-0"}>
                    <Button
                        className={"login-form-button"}
                        variant="outlined"
                    >
                        Зарегистрироваться
                    </Button>
                    <Button
                        className={"login-form-button"}
                        variant="text"
                        onClick={goToLogin}
                    >
                        Войти
                    </Button>
                </div>
            </div>
        </form>
    );
};

export default FormRegistration;