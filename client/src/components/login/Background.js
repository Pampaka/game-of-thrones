import React, {useEffect, useMemo, useRef, useState} from 'react';
import bgVideo from "../../assets/fire.mp4";
import bgAudio from "../../assets/mainTheme.mp3";
import {IconButton, Slider} from "@mui/material";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faVolumeLow, faVolumeHigh, faVolumeXmark, faPlay} from "@fortawesome/free-solid-svg-icons";
import bgLogo from "../../assets/logo.png";

const Background = () => {
    const refAudio = useRef();
    const [pause, setPause] = useState(true);
    const [volume, setVolume] = useState({
        nowVolume: 0,
        lastVolume: 0.5
    });
    const iconAudio = useMemo(() => {
        if (pause) {
            return faPlay;
        }
        if (volume.nowVolume === 0) {
            return faVolumeXmark;
        }
        if (volume.nowVolume < 0.6) {
            return faVolumeLow;
        }
        if (volume.nowVolume >= 0.6) {
            return faVolumeHigh;
        }
    }, [pause, volume])

    const playAudio = () => {
        if (pause) {
            setVolume({
                ...volume,
                nowVolume: volume.lastVolume
            });
            refAudio.current.play();
        } else {
            setVolume({
                nowVolume: 0,
                lastVolume: volume.nowVolume
            });
            refAudio.current.pause();
        }
        setPause(!pause);
    }

    const handleVolume = (e) => {
        setVolume({
            ...volume,
            nowVolume: e.target.value / 100
        });
    }

    useEffect(() => {
        refAudio.current.volume = volume.nowVolume;
        if (volume.nowVolume > 0 && refAudio.current.paused) {
            setPause(!pause);
            refAudio.current.play();
        }
    }, [volume.nowVolume])

    return (
        <div className={"login-background"}>
            <div className={"login-logo"}>
                <img src={bgLogo} alt={"Игра престолов"}/>
            </div>
            <video className={"login-background-video"} autoPlay muted loop poster={bgVideo}>
                <source src={bgVideo} type={"video/mp4"}/>
                Ваш браузер не поодерживает HTML5 видео.
            </video>
            <div className={"login-audio"}>
                <audio src={bgAudio} ref={refAudio} loop>
                    Ваш браузер не поодерживает HTML5 аудио.
                </audio>
                {!pause &&
                    <Slider
                        className={"login-audio-volume"}
                        size="small"
                        defaultValue={0}
                        aria-label="Small"
                        value={volume.nowVolume * 100}
                        onChange={handleVolume}
                    />
                }
                <IconButton
                    className={"login-audio-button"}
                    aria-label="delete"
                    size="large"
                    onClick={playAudio}
                >
                    <FontAwesomeIcon icon={iconAudio} color={"#ffffff"}/>
                </IconButton>
            </div>
        </div>
    );
};

export default Background;