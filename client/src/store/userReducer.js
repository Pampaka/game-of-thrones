import {createSlice} from "@reduxjs/toolkit";

export const userReducer = createSlice({
    name: "user",
    initialState: {
        nickname: "",
        role: null,
        level: null,
        progressLevel: 0,
    },
    reducers: {

    }
})